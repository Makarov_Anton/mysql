<?php


$mark = "Удовлетворительно";
    $text = "Неизвестный чел";

  $image = imagecreatetruecolor(522, 700);
  $backcolor = imagecolorallocate($image, 255, 224, 221);
  $textcolor = imagecolorallocate($image, 50, 50, 50);
  $boxFile = __DIR__ . '/image1.jpg';
  if (!file_exists($boxFile)) {
    echo "Файл с картинкой не найден";
    exit();
  }
  $fontFile = __DIR__ . '/arial.ttf';
  if (!file_exists($fontFile)) {
    echo "Файл со шрифтом не найден";
    exit();
  }
  $imBox = imagecreatefromjpeg($boxFile);
  imagecopy($image, $imBox, 0, 0, 0, 0, 522, 700);
  imagettftext($image, 30, 0, 120, 330, $textcolor, $fontFile, $text);
  imagettftext($image, 16, 0, 260, 460, $textcolor, $fontFile, $mark);
  header('Content-type: image/jpg');
  imagejpeg($image);
