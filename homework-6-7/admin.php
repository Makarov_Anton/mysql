<?php
$quest = ["name", "question", "answer", "true"];
$path = __DIR__ . "/test/";
if (isset($_FILES) && !empty($_FILES)) {
  $array = reArrayFiles($_FILES['userfile']);
  foreach ($array as $file) {
    $uploadfile = basename($file['name']);
    $file_extension = pathinfo($file['name'], PATHINFO_EXTENSION);


    if ($file['error'] == UPLOAD_ERR_OK && $file_extension == "json") {

      $content = file_get_contents($file['tmp_name']);
      $content_example = file_get_contents(__DIR__ . "/test.json");
      $tmp = json_decode($content, true);
      $example = json_decode($content_example, true);

      check($quest, $tmp);
      check_str($tmp, $example, $quest);


      $way = $path . $uploadfile;
      if (!move_uploaded_file($file['tmp_name'], $way)){
        echo "Ошибка. Фаил не был перемещён.";
      }else{
        echo " Фаил перемещён успешно.<br>";
      }

    }else {
      echo "<p style='color:red'> ОШИБКА - {$file['name']} НЕ ЗАГРУЖЕН </p>";
    }
  }
}

function check_str($tmp, $example){
  $true = array();
  foreach (array_keys($tmp) as $tmp_key){
    foreach ($tmp[$tmp_key] as $k => $v){
      foreach ($example as $key => $value){
        if ($k == $key ){
          $true[] = $k;
        }
      }
    }
    $true = array_unique($true);
    if (count($true) != count($example)){
      die("Вложенность массива не совпадает с шаблоном, проверьте массив на повторение ключей");
    }
  }
  echo "Вложенность массива совпадает с шаблоном.<br>";
}

function check($quest, $tmp){
  $count_quest = count($tmp);
  $count_tmp = count($quest);
  if ($count_quest != $count_tmp) {
    echo "Проверьте количесво полей. Пример : <br>";
    help($quest);
    exit;
  }
  foreach ($tmp as $value){
    foreach ($value as $key => $values){
      if (!in_array($key,$quest)) {
        echo "Проверьте структуру файла! Ключ<b> [" . $key . "] </b>не верен!" . "<br> Ознакаомьтесь с разрешённомы ключами и их последовательность:<br>";
        help($quest);
        exit;
      }
    }
  }
  echo "Структуру файла верна.<br>";
  echo "Количесво полей верно.<br>";
}

function help($quest){
  foreach ($quest as $k => $v){
    echo "[" . $k . "] => " . $v . "<br>";
    if ($v == "answer"){
      echo "--где array('answer') - массив с ответами<br></p>";
    }
  }
}

function reArrayFiles($files) {
  $array = array();
  $count = count($files['name']);
  $keys = array_keys($files);

  for($i = 0; $i < $count; $i++) {
    foreach($keys as $key) {
      $array[$i][$key] = $files[$key][$i];
    }
  }

  return $array;
}


?>

<!doctype html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <title>download form</title>
</head>
<body>
<div style="font-weight: bold; font-size: 18px; color: #1e7e34; margin: 20px;">
  <a href="list.php">К загруженным тестам!</a>
</div>
<form action=" " method="post" enctype="multipart/form-data">
  <input type="file" name="userfile[]"><br>
  <input type="submit" value="Загрузить"><br>
</form>
</body>
</html>